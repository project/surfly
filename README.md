# Introduction
As described in the Surfly website, Surfly.com allows you to
* Share your browser in real time
* Upgrade a phone conversation with a video chat
* Guide your customers & Draw to highlight
* Share documents securely

# About this module
* This module only embeds the default code from surfly.com to your Drupal website and doesn't use the REST API of the service.
* The options of the script are set from the Surfly dashboard and not from your Drupal installation.
* The Surfly Javascript code doesn't require jQuery; neither this module does.
* This module isn't affiliated with Surfly in any way.

# Installation
Install the module as you would any other Drupal module.

# Configuration
1. As a first step you should go to surfly.com and register for an account. After registration, an API key will be automatically generated for you.
2. Go to your website at admin/config/system/surfly and paste your API key.
3. Optionally, set some paths you would like the surfly script to be limited or excluded.
4. Go to your surfly.com dashboard (Settings -> Options) and configure your surfly button and session options accordingly.

# Credits
vensires (http://drupal.org/u/vensires)