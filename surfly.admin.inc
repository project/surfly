<?php

/**
 * @file
 * Contains administrative functions.
 */

/**
 * Administrative settings form.
 */
function surfly_admin_settings_form($form, $form_state) {
  $form['surfly_widgetkey'] = array(
    '#type' => 'textfield',
    '#title' => t('Widget Key'),
    '#description' => t('You can obtain your Widget Key from your @url profile by visiting the %tab tab', array(
      '@url' => 'surfly.com',
      '%tab' => t('Integration'),
    )),
    '#default_value' => variable_get('surfly_widgetkey'),
    '#required' => TRUE,
  );

  $form['visibility'] = array(
    '#type' => 'fieldset',
    '#title' => t('Pages'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    'surfly_visibility' => array(
      '#type' => 'radios',
      '#title' => t('Include script on specific pages'),
      '#options' => array(
        SURFLY_VISIBILITY_NOT_LISTED => t('All pages except those listed'),
        SURFLY_VISIBILITY_LISTED => t('Only the listed pages'),
      ),
      '#default_value' => variable_get('surfly_visibility', SURFLY_VISIBILITY_NOT_LISTED),
    ),
    'surfly_pages' => array(
      '#type' => 'textarea',
      '#title' => '<span class="element-invisible">' . t('Pages') . '</span>',
      '#default_value' => variable_get('surfly_pages', ''),
      '#description' => t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", array('%blog' => 'blog', '%blog-wildcard' => 'blog/*', '%front' => '<front>')),
    ),
  );
  return system_settings_form($form);
}
